package demo.pos.domain.sale.discount;

import demo.pos.common.Interval;
import demo.pos.domain.sale.Sale;

import java.time.*;
import java.util.logging.Logger;

/**
 * @author Jan de Rijke.
 */
public class SaleDiscount implements Discount {
	private double discount;
	private Interval period;

	public SaleDiscount(Interval period, double discount) {
		this.period=period;
		this.discount = discount;
	}

	@Override
	public double getDiscount(double total) {
		return ( period.contains (LocalDate.now()))?total*discount:0.0;
	}
}
