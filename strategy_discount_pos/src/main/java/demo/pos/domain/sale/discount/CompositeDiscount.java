package demo.pos.domain.sale.discount;

import java.util.ArrayList;
import java.util.List;

public class CompositeDiscount implements Discount{
	List<Discount> discounts=new ArrayList<>();
	@Override
	public double getDiscount(double amount) {
		double reduced = amount;
		for(Discount discount : discounts){
			reduced += discount.getDiscount(reduced);
		}
		// discounts are negative numbers!
		return reduced - amount;
	}

	public void addDiscount(Discount discount){
		discounts.add(discount);
	}

	public void removeDiscount(Discount discount){
		discounts.remove(discount);
	}
}
