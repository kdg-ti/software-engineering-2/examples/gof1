package demo.pos.persistence;

import java.util.logging.Logger;

import static demo.pos.persistence.PersistenceType.MEMORY;

/**
 * @author Jan de Rijke.
 */
public class Repositories {
  private static Logger logger = Logger.getLogger("demo.pos.application.Pos");
  private  static RepositoryFactory repositories;

  public static void init(PersistenceType persistence){
  	switch (persistence){
		  case MEMORY: repositories =  new MemoryRepositoryFactory();	break;
		  case DB: repositories = new DbRepositoryFactory();break;
	  }
  }

  public  static RepositoryFactory getRepositories(){
  	if (repositories == null){
  		init(MEMORY);
	  }
  	return repositories;
  }




}
