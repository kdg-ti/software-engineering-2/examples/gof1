package demo.pos.application;

import demo.pos.business.ProductService;
import demo.pos.business.SaleService;
import demo.pos.domain.sale.Sale;
import demo.pos.domain.sale.discount.Discount;

/**
 * Created by overvelj on 14/11/2016.
 */
public class PosController {
	private ProductService catalog=new ProductService();
	private SaleService store=new SaleService();


	public void enterItem(long saleId, long id, int qty) {
		store.addSalesLine(saleId, id, qty);
	}

	public SaleService getStore() {
		return store;
	}



	public void endSale(long saleId) {
		store.endSale(saleId);
	}
	public void makePayment(long saleId,double i) {
		store.makePayment(saleId,i);
	}

	public Sale makeNewSale() {
		return store.addSale();
	}

	public Sale getSale(long id) {
		return store.getSale(id);
	}

	public void setDiscount(long saleId,Discount promo) {
		store.getSale(saleId).setDiscount(promo);
	}
}
