package domain;

import publisher.Publisher;
import publisher.Subscriber;

import java.util.ArrayList;
import java.util.List;


public class Point implements Publisher {
    private final List<Subscriber> subscribers  = new ArrayList<>();
    private int x;
    private int y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void doubleX() {
        x *= 2;
        this.notifySubscribers("doubled X");
    }

      private void notifySubscribers(String action) {
        subscribers.forEach(s -> s.update(action, this));
      }

    public void doubleY() {
        y *= 2;
        this.notifySubscribers("doubled Y");
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }
}
