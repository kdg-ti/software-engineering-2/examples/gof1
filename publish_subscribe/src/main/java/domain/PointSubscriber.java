package domain;

import publisher.Subscriber;

public class PointSubscriber implements Subscriber {


    @Override
    public void update(String event, Object source) {
        System.out.println("Event: " + event + " Point: " + source);

    }
}