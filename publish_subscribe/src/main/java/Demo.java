import domain.Point;
import domain.PointSubscriber;

public class Demo {
    public static void main(String[] args) {
        Point point = new Point(1, 2);
        System.out.println("Starting point: " + point);
        PointSubscriber subscriber = new PointSubscriber();
        point.subscribe(subscriber);
        point.doubleX();
        point.doubleY();
    }
}
