package composite;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
/**
 * Composite pattern demo.
 * Implements a nested, recursive structure for SoftwareProjects that can consist of
 *  ProjectTasks and sub-SoftwareProjects (which can have subProjects and tasks themselves)
 */
class SoftwareProjectTest {
  ProjectTask task1, task2, task3;
  SoftwareProject subProject, masterProject;

  @BeforeEach
  public void setUp() {
    // given - arrange
    task1 = new ProjectTask("Mark", "Write JUnit tests ", LocalDate.of(2020, 10, 10), 16);
    task2 = new ProjectTask("Linda", "Webservices", LocalDate.of(2020, 10, 5), 4);
    task3 = new ProjectTask("Freddy", "Website frontend", LocalDate.of(2020, 10, 15), 64);

    subProject = new SoftwareProject("Web application City of Antwerp");
    subProject.add(task1);
    subProject.add(task2);
    subProject.add(task3);

    masterProject = new SoftwareProject("Project City of Antwerp");
    masterProject.add(subProject);
    masterProject.add(new ProjectTask("Nancy",
        "Prepare project proposal",
        LocalDate.of(2020, 11, 5),
        4));

  }

  @Test
  void getTime() {
    assertEquals(88,
        masterProject.getTime(),
        () ->  String.format("Total time for %s should be %d ", masterProject, 88));
  }


  @Test
  void getChildWithInexistantIndexShouldThrowException() {
    masterProject = new SoftwareProject("Project City of Antwerp");
    assertThrows(IndexOutOfBoundsException.class, () -> masterProject.getChild(1));
  }

  @Test
  @DisplayName("Given I use an index equal to the number Of Children When I retrieve the child Then I should get an exception")
  void getLastChild() {
    // when - act
    Executable getChild = () -> masterProject.getChild(2);
    // then - assert
    assertThrows(IndexOutOfBoundsException.class, getChild);
  }


}