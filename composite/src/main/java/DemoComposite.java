import composite.ProjectTask;
import composite.SoftwareProject;

import java.time.LocalDate;



public class DemoComposite {
   //demo this using unit test

	 public static void main(String[] args) {
   ProjectTask task1=new ProjectTask("Mark",
     "Schrijf JUnit testen", LocalDate.of(2016, 10, 10), 16);
   ProjectTask task2 = new ProjectTask("Linda", "Webservices"			, LocalDate.of(2016, 10, 5), 4);
   ProjectTask task3 = new ProjectTask("Freddy",
     "Website frontend", LocalDate.of(2016, 10, 15), 64);
   SoftwareProject subProject =
     new SoftwareProject("Web applicatie Stad Antwerpen");
   subProject.add(task1);
   subProject.add(task2);
   subProject.add(task3);
   SoftwareProject masterProject =
     new SoftwareProject("Project Stad Antwerpen");
   masterProject.add(subProject);
   masterProject.add(new ProjectTask("Nancy", "Offerte opmaken"		, LocalDate.of(2016, 11, 5), 4));
   System.out.print(masterProject);
   System.out.println("Totaal begroot: " +  masterProject.getTime() 														+ " uren");
    }
}


